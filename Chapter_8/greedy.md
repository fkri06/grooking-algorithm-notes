## Greedy Algorithm

Algorithm that pick the most optimal solution **at each step** in order to find the desired solution; this optimal solution at each step is called *locally optimal* and desired solution is called *globally optimal*. Sometimes it doesn't yield the most optimal solution in the end.

### Problem Examples:

- The Classroom Scheduling Problem
- The Knapsack Problem
- Coin Exchange Problem
- The Set Covering Problem




