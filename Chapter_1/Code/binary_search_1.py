def binary_search(haystack: list, needle: int) -> bool:
    low = 0
    high = len(haystack) - 1
    
    while low <= high:
        mid = (low + high) // 2 # rounded down if low + high is not an even number.
        guess = haystack[mid]

        if guess == needle:
            return True
        elif guess < needle:
            low = mid + 1
        else:
            high = mid - 1

    return False

list_of_numebers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
search_value = 15
print(f"Does the value {search_value} exist in the list? {binary_search(list_of_numebers, search_value)}")