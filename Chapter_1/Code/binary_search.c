#include <stdio.h>

int binary_search(int *haystack, int length, int needle) {
	int low = 0;
	int high = length - 1;

	while(low <= high) {
		int mid = low + (high - low) / 2;
		int mid_value = haystack[mid];

		if(needle == mid_value) {
			return 1;
		} else if(needle < mid_value) {
			high = mid - 1;
		} else {
			low = mid + 1;
		}
	}
	return 0;
}

int main() {
    int array[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int length = sizeof(array) / sizeof(array[0]);
	
	if(binary_search(array, length, 10)) {
		printf("Found\n");
	} else {
		printf("Not found\n");
	}
}
