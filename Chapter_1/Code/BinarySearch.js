function binarySearch(haystack, needle) {
    let low = 0;
    let high = haystack.length - 1;

    while (low <= high) {
        let mid = Math.floor(low + (high - low) / 2);
        let midValue = haystack[mid];

        if (needle === midValue) {
            return {
                "searchStatus": "Found",
                "atIndex": mid,
                "value": midValue
            }
        } else if (needle < midValue) {
            high = mid - 1;
        } else {
            low = mid + 1;
        }
    }
    return { "searchStatus": "Not Found" };
}

let listNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

console.log(binarySearch(listNumbers, -1));
console.log(binarySearch(listNumbers, 1));
console.log(binarySearch(listNumbers, 2));
console.log(binarySearch(listNumbers, 3));
console.log(binarySearch(listNumbers, 4));
console.log(binarySearch(listNumbers, 5));
console.log(binarySearch(listNumbers, 6));
console.log(binarySearch(listNumbers, 7));
console.log(binarySearch(listNumbers, 8));
console.log(binarySearch(listNumbers, 9));
console.log(binarySearch(listNumbers, 10));
console.log(binarySearch(listNumbers, 11));
