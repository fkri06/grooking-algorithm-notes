## Introduction to Algorithms
- A set of rules/instructions for some computational tasks.

- Some example of common algorithms used to solve problems:
    - Finding the shortest path in a google map.
    - Algorithm to solve games like 15-puzzle, checkers, or n-queen problems in chess.
    - Searching or sorting some list of items.
    - Algorithm that recommend items in an online shopping platform.

## Binary Search
- A method to search an item in a **sorted** list of items by **eliminating** half of the data in the list until it reaches the item or the item cannot be found.
- Only works when the list is sorted.

Suppose we have a list of numbers `[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]` and we want to search the value `3` in the list.
```
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

## Pick the value in the middle; 6.

## The value we're looking for is 3, it's less than our middle value 6, so we know after the fact that this list is an ordered list, and that our value must be on the other half of the list where all the items are less than the value in the middle 6.

[1, 2, 3, 4, 5]

We repeat the same processes, now our middle value is 3, which is the value we're looking for, so we're done searching. We have 10 items in a list but we found the value 3 with only 3 steps.
```
### Linear vs Binary Search Running Time In a **Worst Case** scenario
|  # of items|       Binary Search         |   Linear Search    |
| :--------- | :-------------------------: | :---------------:  |
|     1      |            0                |          1         |
|     2      |            1                |          2         |
|     4      |            2                |          4         |
|     8      |            3                |          8         |
|     16     |            4                |          16        |
|     32     |            5                |          32        |
|     64     |            6                |          64        |
|    128     |            7                |          128       |
|    256     |            8                |          256       |
|  240,000   |           18                |      240,000       |

Everytime we double the value in binary search, we add a step. On the other hand, we would have the same number of steps relative to the number of items in linear search. 

- Binary search has a running time of binary logarithm; Log<sub>2</sub>(N) where N is the number of items in a list.
    - For 256 items in a list, binary search will have 8 running time/steps at most.
- Linear search has a linear running time; N.
    - For 256 items in a list, linear search will have 256 running time/steps at most.

## Big O Notation
- Tools to help us understand the running time/steps required for an algorithm, when the input(number of items) grows, or how many steps increase when the input increase.

- Running time = operations = steps. 

- Big O measure the worst case.
    - The worst case for searching an item in sorted list is when the value we're looking for at the end of the list, or not to be found in the list.

- Common big O runtimes:

    - O(log n) -> logarithmic time: binary search.

    - O(n) -> linear time: linear search.
    
    - O(n * log n) -> quicksort.

    - O(n<sup>2</sup>) -> quadratic time: simple matrix multiplication, selection sort.

    - O(n!) -> factorial time: TSP problem.



## Exercises Answers
1.1. The running time for a 128 sorted list of name would be log<sub>2</sub>(128) which is 7. The maximum number to search through 128 names would be 7.

1.2. If we double 128 and now it's 256 we would just add one step to the maximum number of steps required to run the program, which is 8.

1.3. O(n) for linear search, O(log n) for binary search.

1.4. O(n)

1.5. O(n)
