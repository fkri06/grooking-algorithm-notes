# Grooking-Algorithm-Notes

This is a collection of my personal notes, code, and
exercise answers from [Grokking Algorithms](https://www.amazon.com/Grokking-Algorithms-illustrated-programmers-curious/dp/1617292230) book by [Aditya Y. Bhargava](https://www.adit.io/). It is not a complete notes of the book's content because I know some of algorithm in the book, I have completed a semester class on design and analysis of algorithm, but I feel like the content is too abstract and not particularly easy to follow in my opinion, so I decided to read this book to revisit and to fill some of that gap. **Please note** that this is a work in-progress repo.

## [Chapter 1: Introduction to Algorithms](./Chapter_1/Introduction_to_algorithm.md)
- ### [Introduction](./Chapter_1/Introduction_to_algorithm.md#introduction-to-algorithms)
- ### [Binary Search](./Chapter_1/Introduction_to_algorithm.md#binary-search)
- ### [Big O](./Chapter_1/Introduction_to_algorithm.md#big-o-notation)
- ### [Code](./Chapter_1/Code/)

## [Chapter 2: Selection Sort](./Chapter_2/Selection_sort.md)
- ### [Arrays and Linked List](./Chapter_2/Selection_sort.md#arrays-and-linked-list)
- ### [Binary Search](./Chapter_2/Selection_sort.md#selection-sort)
- ### [Code](./Chapter_2/Code/)

## [Chapter 3: Recursion](./Chapter_3/recursion.md)
- ### [Recursion](./Chapter_3/recursion.md#recursion)
- ### [Cases](./Chapter_3/recursion.md#base-case--recursive-case)
- ### [Stack](./Chapter_3/recursion.md#stack)
- ### [Code](./Chapter_3/Code/)

## [Chapter 4: Recursion](./Chapter_4/quicksort.md)
- ### [Divide & Conquer](./Chapter_4/quicksort.md#divide--conquer)
- ### [Quicksort](./Chapter_4/quicksort.md#quicksort)
- ### [Code](./Chapter_4/Code/)

## [Chapter 5: Hash Tables](./Chapter_5/hash_table.md#hash-table)
- ### [Hash Function](./Chapter_5/hash_table.md#hash-function)
- ### [Collision](./Chapter_5/hash_table.md#collision)
- ### [Load Factor](./Chapter_5/hash_table.md#collision)

## [Chapter 6: Breadth First Search](./Chapter_6/BFS.md)
- ### [Graphs](./Chapter_6/BFS.md#graphs)
- ### [Breadth First Search](./Chapter_6/BFS.md#breadth-first-search)
- ### [Breadth First Search](./Chapter_6/Code/)

## [Chapter 7: Dijkstra's Algorithm](./Chapter_7/dijkstra_algorithm.md)
- ### [Shortest Path Problem](./Chapter_7/dijkstra_algorithm.md#shortest-path-problem)
- ### [Dijkstra's Algorithm](./Chapter_7/dijkstra_algorithm.md#dijkstras-algorithm)
- ### [Code](./Chapter_7/Code/)

## [Chapter 8: Greedy Algorithm](./Chapter_8/greedy.md#greedy-algorithm)
- ### [Greedy Problems](./Chapter_8/greedy.md#problem-examples)
- ### [Code](./Chapter_8/Code/)

## [Chapter 9: Dynamic Programming](./Chapter_9/dynamic_programming.md#dynamic-programming)
- ### [Code](./Chapter_9/Code/)

...other chapters