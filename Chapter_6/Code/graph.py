# OOP implementation of a graph

class Vertex:
    def __init__(self, value = None):
        self.value = value # vertex label; could be person's name or integer value
        self.adjacent_vertices = [] # store all vertices that connect to **self** vertex
    
    def add_adjacent_vertex(self, vertex_to_add):
        if vertex_to_add in self.adjacent_vertices:
            return
        self.adjacent_vertices.append(vertex_to_add)
        vertex_to_add.adjacent_vertices.append(self) # undirected graph
    
    def depth_first_traversal(self, starting_vertex, visited_vertices = {}):
        visited_vertices[starting_vertex.value] = True
        print(starting_vertex.value)
        for adjacent_vertex in starting_vertex.adjacent_vertices:
            if visited_vertices.get(adjacent_vertex.value):
                continue
            self.depth_first_traversal(adjacent_vertex, visited_vertices)
    
    def depth_first_search(self, vertex, vertex_to_search: str, visited_vertices = {}):
        if vertex.value == vertex_to_search:
            return True
        visited_vertices[vertex.value] = True
        for adjacent_vertex in vertex.adjacent_vertices:
            if adjacent_vertex.value == vertex_to_search:
                return True
            if visited_vertices.get(adjacent_vertex.value):
                continue
        return False
    
    def breadth_first_traversal(self, starting_vertex):
        queue = [starting_vertex] # list as queue
        visited_vertices = {}
        visited_vertices[starting_vertex.value] = True
        
        while queue:
            current_vertex = queue.pop(0)
            assert type(current_vertex) == Vertex, f"Not a vertex object ---> {type(current_vertex)}"
            print(current_vertex.value)

            for adjacent_vertex in current_vertex.adjacent_vertices:
                if not visited_vertices.get(adjacent_vertex.value):
                    visited_vertices[adjacent_vertex.value] = True
                    queue.append(adjacent_vertex)
    
    def breadth_first_search(self, starting_vertex, search_vertex: str):
        queue = [starting_vertex] # list as queue
        visited_vertices = {}
        visited_vertices[starting_vertex.value] = True
        
        while queue:
            current_vertex = queue.pop(0)
            assert type(current_vertex) == Vertex, f"Not a vertex object ---> {type(current_vertex)}"
            
            if current_vertex.value == search_vertex:
                return True

            for adjacent_vertex in current_vertex.adjacent_vertices:
                if adjacent_vertex.value == search_vertex:
                    return True
                
                if not visited_vertices.get(adjacent_vertex.value):
                    visited_vertices[adjacent_vertex.value] = True
                    queue.append(adjacent_vertex)
        return False


    def print_neighbors(self):
        print(f"Vertex {self.value} has neighbors: ")
        print([vertex.value for vertex in self.adjacent_vertices])

alice = Vertex("Alice")
bob = Vertex("Bob")
eve = Vertex("Eve")
gary = Vertex("Gary")
anna = Vertex("Anna")

alice.add_adjacent_vertex(bob)
alice.add_adjacent_vertex(eve)
alice.add_adjacent_vertex(anna)

bob.add_adjacent_vertex(eve)

eve.add_adjacent_vertex(gary)
anna.add_adjacent_vertex(eve)

gary.add_adjacent_vertex(alice)

graph = Vertex()
print("Depth First Traversal")
graph.depth_first_traversal(alice)
print(graph.depth_first_search(alice, "Gary"))
print(graph.depth_first_search(alice, "Maya"))
print("Breadth Frist Traversal")
graph.breadth_first_traversal(alice)
print(graph.depth_first_search(alice, "Gary"))
print(graph.depth_first_search(alice, "Maya"))
