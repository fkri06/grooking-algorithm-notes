# weighted directed graph
# represented by dictionary and list

def create_vertex(value: str) -> dict:
    return {"value": value, "adjacent_vertices": []}

def add_adjacent_vertex(vertex: dict, to_vertex: dict, weight: int):
    if to_vertex in vertex["adjacent_vertices"]:
        return
    create_connection = {"to": to_vertex["value"], "weight": weight}
    vertex["adjacent_vertices"].append(create_connection)

def remove_adjacent_vertex(vertex: dict, vertex_to_remove: dict):
    for v in vertex["adjacent_vertices"]:
        if v["to"] == vertex_to_remove["value"]:
            vertex["adjacent_vertices"].remove(v)
            return

if __name__ == "__main__":
    norman = create_vertex("norman")
    emma = create_vertex("emma")
    ray = create_vertex("ray")
    anna = create_vertex("anna")
    bob = create_vertex("bob")

    add_adjacent_vertex(norman, emma, 5)
    add_adjacent_vertex(norman, ray, 7)
    add_adjacent_vertex(emma, anna, 15)
    add_adjacent_vertex(bob, norman, 10)
    add_adjacent_vertex(ray, bob, 20)

    print(norman)
    print(emma)
    print(ray)
    print(anna)
    print(bob)



