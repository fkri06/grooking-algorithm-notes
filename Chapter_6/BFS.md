## Graphs

* A data structure where elements are called vertices, and each elements are connected to one another through
a link called edges.
* Vertices that are connected with a vertex are called neighbors.
* Graphs are used to model connections.
* Example connections in social media networks:
    * A following B, B folowing C, and C following A.
* Directed and Undirected graph using social media example:
    * (**Directed**) A following B, but B doesn't necessarily follow back A. It means vertex A has single directed edge to B.
    * (**Undirected**) A following B, and B has to follow back A.

## Breadth First Search

* Algorithm to solve the [*shortest-path*](https://en.wikipedia.org/wiki/Shortest_path_problem) problem.
