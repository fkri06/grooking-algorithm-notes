## Shortest Path Problem

* A problem in graph theory to find the minimum path from source vertex to destination vertex.
* Algorithm that can solve this problem:
    * Dijkstra’s Algorithm
    * Bellman-Ford Algorithm

## Dijkstra's Algorithm

* This algorithm only works on Directed Acyclic Graphs, and non negative weight.

## Case Study

Find the shortest path from the following map:

1. Exercise 1

![City Map](./Code/Screenshot_2024-07-04_11-43-03.png)

2. Exercise 2

![City Map](./Code/1200px-Shortest_path_with_direct_weights.svg.png)

<div style="text-align: center"><a href="https://upload.wikimedia.org/wikipedia/commons/3/3b/Shortest_path_with_direct_weights.svg">Source: Wikipedia</div>

3. Exercise 3

![City Map](./Code/Fig11.16.png)

<div style="text-align: center"><a href="https://cgi.luddy.indiana.edu/~yye/c343-2019/shortest.php">Source: cgi.luddy.indiana.edu</div>

4. Exercise 4

![City Map](./Code/Screenshot_2024-07-09_09-58-02.png)

<div style="text-align: center"><a href="https://informatika.stei.itb.ac.id/~rinaldi.munir/Stmik/2020-2021/Program-Dinamis-2020-Bagian1.pdf">Source: informatika.stei.itb.ac.id</div>


### Solution Code
[Solution For Exercise 1](./Code/shortest_path.py)

[Solution For Exercise 2](./Code/dijkstra_exercise.py)

[Solution For Exercise 3](./Code/dijkstra_exercise2.py)

[Solution For Exercise 4](./Code/dijkstra_exercise3.py)
