graph = {}
graph["A"] = {}
graph["B"] = {}
graph["C"] = {}
graph["D"] = {}
graph["E"] = {}
graph["F"] = {}

# Edges in vertex A - F
graph["A"]["B"] = 4
graph["A"]["C"] = 2
graph["B"]["C"] = 5
graph["B"]["D"] = 10
graph["C"]["E"] = 3
graph["D"]["F"] = 11
graph["E"]["D"] = 4

# costs table
infinity = float("inf")
costs = {}
costs["B"] = 4
costs["C"] = 2
costs["D"] = infinity
costs["E"] = infinity
costs["F"] = infinity

# parents table
parents = {}
parents["B"] = "A"
parents["C"] = "A"
parents["F"] = None

processed = []

def find_next_lowest(costs: dict):
    lowest_cost = float("inf")
    lowest_vertex = None

    for vertex, cost in costs.items():
        if cost < lowest_cost and vertex not in processed:
            lowest_cost = cost
            lowest_vertex = vertex
    return lowest_vertex

def draw_path(starting_vertex: str, destination_vertex: str, parents: dict):
    shortest_path = []
    current_vertex = destination_vertex

    while current_vertex != starting_vertex:
        shortest_path.append(current_vertex)
        current_vertex = parents[current_vertex]
    
    shortest_path.append(starting_vertex)
    return list(reversed(shortest_path))

if __name__ == "__main__":
    vertex = find_next_lowest(costs)

    while vertex != None:
        cost = costs[vertex]
        neighbors: dict = graph[vertex]

        for neighbor, neighbor_cost in neighbors.items():
            new_cost = cost + neighbor_cost
            if new_cost < costs[neighbor]:
                costs[neighbor] = new_cost
                parents[neighbor] = vertex
        
        processed.append(vertex)
        vertex = find_next_lowest(costs)
    
    print(f"Shortest path from A to F = {draw_path('A', 'F', parents)}")