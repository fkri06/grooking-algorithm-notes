class WeightedGraph:
    def __init__(self, name = None):
        self.name = name
        self.routes = {}
    
    def add_route(self, vertex, price: int):
        """
            vertex  : WeightedGraph
            price   : Edge's weight in integer
        """
        self.routes[vertex] = price

def adjacent_vertex_price_exist(table: dict, vertex_name: str):
    """
        Return true if the vertex's price exit in the cheapest price table 
    """
    return table.get(vertex_name)

def update_table(c_price_table: dict, c_prev_table: dict, price: int, curr_vertex: WeightedGraph, adj_vertex: WeightedGraph):
    """
        To update the cheapest price table and the stopover table
    """
    c_price_table[adj_vertex.name] = price
    c_prev_table[adj_vertex.name] = curr_vertex.name

def next_cheapest_unvisited_vertex(c_price_table: dict, unvisited_vertex: list) -> WeightedGraph:
    """
        Finding the next cheapest vertex to visit;
        return the list if unvisited_vertex empty.
    """
    if not unvisited_vertex:
        return None
    
    min_vertex = unvisited_vertex[0]
    for i in range(1, len(unvisited_vertex)):
        if c_price_table[unvisited_vertex[i].name] < c_price_table[min_vertex.name]:
            min_vertex = unvisited_vertex[i]
    return min_vertex
    

def dijkstra_shortest_path(starting_vertex: WeightedGraph, destination_vertex: WeightedGraph):
    cheapest_price_table = {}
    cheapest_prev_stopover_table = {}

    unvisited_vertex = []
    visited_vertex = {}

    cheapest_price_table[starting_vertex.name] = 0
    current_vertex = starting_vertex

    while current_vertex:

        visited_vertex[current_vertex.name] = True
        try:
            unvisited_vertex.remove(current_vertex)
        except ValueError:
            pass

        for adjacent_vertex, price in current_vertex.routes.items():

            # found new vertices
            if not visited_vertex.get(adjacent_vertex.name):
                unvisited_vertex.append(adjacent_vertex)
            
            price_through_current_vertex = cheapest_price_table[current_vertex.name] + price

            # Compare cheapest price path from starting vertex to current vertex
            if adjacent_vertex_price_exist(cheapest_price_table, adjacent_vertex.name):
                if price_through_current_vertex < cheapest_price_table[adjacent_vertex.name]:
                    update_table(
                        cheapest_price_table, 
                        cheapest_prev_stopover_table,
                        price_through_current_vertex,
                        current_vertex,
                        adjacent_vertex
                    )
            else:
                 update_table(
                        cheapest_price_table, 
                        cheapest_prev_stopover_table,
                        price_through_current_vertex,
                        current_vertex,
                        adjacent_vertex
                    )
        
        current_vertex = next_cheapest_unvisited_vertex(cheapest_price_table, unvisited_vertex)

    # draw the paths
    shortest_path = []
    current_vertex_name = destination_vertex.name

    while current_vertex_name != starting_vertex.name:
        shortest_path.append(current_vertex_name)
        current_vertex_name = cheapest_prev_stopover_table[current_vertex_name]
    
    shortest_path.append(starting_vertex.name)
    return list(reversed(shortest_path))
        


if __name__ == "__main__":
    A = WeightedGraph("A")
    B = WeightedGraph("B")
    C = WeightedGraph("C")
    D = WeightedGraph("D")

    A.add_route(B, 6)
    A.add_route(C, 2)
    B.add_route(D, 1)
    C.add_route(B, 3)
    C.add_route(D, 5)

    print(f"Shortest path from {A.name} to {D.name} = {dijkstra_shortest_path(A, D)}")
    