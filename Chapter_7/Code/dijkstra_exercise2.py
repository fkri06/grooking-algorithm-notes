graph = {}
graph["A"] = {}
graph["B"] = {}
graph["C"] = {}
graph["D"] = {}
graph["E"] = {}

graph["A"]["B"] = 10
graph["A"]["C"] = 3
graph["B"]["D"] = 5
graph["C"]["B"] = 2
graph["C"]["E"] = 15

# Costs table
infinity = float("inf")
costs = {}
costs["B"] = 10
costs["C"] = 3
costs["D"] = infinity
costs["E"] = infinity

# Parents table
parents = {}
parents["B"] = "A"
parents["C"] = "A"
parents["D"] = None

processed = []

def find_lowest_vertex(costs: dict):
    lowest_cost = float("inf")
    lowest_vertex = None

    for vertex, vertex_cost in costs.items():
        if vertex_cost < lowest_cost and vertex not in processed:
            lowest_cost = vertex_cost
            lowest_vertex = vertex
    
    return lowest_vertex

def draw_path(src: str, dst: str, parents: dict):
    shortest_path = []
    current = dst

    while current != src:
        shortest_path.append(current)
        current = parents[current]
    
    shortest_path.append(src)
    return list(reversed(shortest_path))
    
if __name__ == "__main__":
    vertex = find_lowest_vertex(costs)

    while vertex != None:
        cost = costs[vertex]
        vertex_neighbors: dict = graph[vertex]

        for neighbor, neighbor_cost in vertex_neighbors.items():
            new_cost = cost + neighbor_cost
            if new_cost < costs[neighbor]:
                costs[neighbor] = new_cost
                parents[neighbor] = vertex

        processed.append(vertex)
        vertex = find_lowest_vertex(costs)
    
    print(f"Shortest path from A to D: {draw_path('A', 'D', parents)}")