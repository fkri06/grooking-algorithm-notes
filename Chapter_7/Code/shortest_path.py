graph = {}
graph["banyumas"] = {}
graph["semarang"] = {}
graph["wonosobo"] = {}
graph["pekalongan"] = {}
graph["solo"] = {}
graph["pemalang"] = {}

graph["banyumas"]["semarang"] = 5
graph["banyumas"]["wonosobo"] = 2
graph["semarang"]["pekalongan"] = 4
graph["semarang"]["solo"] = 2
graph["wonosobo"]["solo"] = 7
graph["wonosobo"]["semarang"] = 8

graph["pekalongan"]["solo"] = 6
graph["pekalongan"]["pemalang"] = 3
graph["solo"]["pemalang"] = 1

# costs table: to record the lowest costs from 
# starting city to all other city, including destination city
# in the network
infinity = float("inf")
costs = {}
costs["semarang"] = 5
costs["wonosobo"] = 2
costs["solo"] = infinity
costs["pekalongan"] = infinity
costs["pemalang"] = infinity

# parents table: to record the shortest path
# from starting city to the destination
parents = {}
parents["semarang"] = "banyumas"
parents["wonosobo"] = "banyumas"
parents["pemalang"] = None

processed_city = []

def lowest_cost_city_to_visit_next(costs: dict):
    lowest_cost = float("inf")
    lowest_cost_city = None
    for city, city_cost in costs.items():
        cost = city_cost
        if cost < lowest_cost and city not in processed_city:
            lowest_cost = cost
            lowest_cost_city = city
    return lowest_cost_city

def draw_path(parent_records: dict, starting_city: str, destination_city: str):
    shortest_path = []
    current_city = destination_city
    
    while current_city != starting_city:
        shortest_path.append(current_city)
        current_city = parent_records[current_city]
    shortest_path.append(starting_city)
    return list(reversed(shortest_path))


if __name__ == "__main__":
    city = lowest_cost_city_to_visit_next(costs)

    # while we still have city to be processed
    while city != None:
        cost = costs[city]
        neighbors: dict = graph[city]

        # check current city adjacent cities
        for neighbor_city in neighbors.keys():
            new_cost = cost + neighbors[neighbor_city]
            if new_cost < costs[neighbor_city]:
                costs[neighbor_city] = new_cost
                parents[neighbor_city] = city

        processed_city.append(city)
        city = lowest_cost_city_to_visit_next(costs)

    starting_city = "banyumas"
    destination_city = "pemalang"
    print(f"Shortest path to go from Banyumas to Pemalang: {draw_path(parents, starting_city, destination_city)}")

