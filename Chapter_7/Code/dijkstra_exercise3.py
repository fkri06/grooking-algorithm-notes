graph = {}
graph["one"] = {}
graph["two"] = {}
graph["three"] = {}
graph["four"] = {}
graph["five"] = {}
graph["six"] = {}
graph["seven"] = {}
graph["eight"] = {}
graph["nine"] = {}
graph["ten"] = {}

# edges from each graph vertex
graph["one"]["two"]      = 2
graph["one"]["three"]    = 4
graph["one"]["four"]     = 3

graph["two"]["five"]     = 7
graph["two"]["six"]      = 4
graph["two"]["seven"]    = 6

graph["three"]["five"]   = 3
graph["three"]["six"]    = 2
graph["three"]["seven"]  = 4

graph["four"]["five"]    = 4
graph["four"]["six"]     = 1
graph["four"]["seven"]   = 5

graph["five"]["eight"]   = 1
graph["five"]["nine"]    = 4

graph["six"]["eight"]    = 6
graph["six"]["nine"]     = 3

graph["seven"]["eight"]  = 3
graph["seven"]["nine"]   = 3

graph["eight"]["ten"]    = 3
graph["nine"]["ten"]     = 4

# costs table
infinity = float("inf")
costs = {}
costs["two"]             = 2
costs["three"]           = 4
costs["four"]            = 3
costs["five"]            = infinity
costs["six"]             = infinity
costs["seven"]           = infinity
costs["eight"]           = infinity
costs["nine"]            = infinity
costs["ten"]             = infinity

# parents table
parents = {}
parents["two"]           = "one"
parents["three"]         = "one"
parents["four"]          = "one"
parents["ten"]           = None

processed = []

def next_lowest_vertex(costs: dict):
    lowest_cost = float("inf")
    lowest_vertex = None

    for vertex, cost in costs.items():
        if cost < lowest_cost and vertex not in processed:
            lowest_cost = cost
            lowest_vertex = vertex
    
    return lowest_vertex

def draw_path(src_vertex:str, dst_vertex:str, parents: dict):
    shortest_path = []
    curr          = dst_vertex

    while curr != src_vertex:
        shortest_path.append(curr)
        curr = parents[curr]
    
    shortest_path.append(src_vertex)
    return list(reversed(shortest_path))
    

if __name__ == "__main__":
    vertex = next_lowest_vertex(costs)
    while vertex != None:
        cost = costs[vertex]    # current vertex cost
        neighbors:dict = graph[vertex] # current vertex adjacent vertices
        
        for adj_vertex, adj_cost in neighbors.items():
            new_cost = cost + adj_cost
            if new_cost < costs[adj_vertex]:
                costs[adj_vertex] = new_cost
                parents[adj_vertex] = vertex

        processed.append(vertex)
        vertex = next_lowest_vertex(costs)
    
    print(f"Shortest path from vertex 1 to 10\t: {draw_path('one', 'ten', parents)}")
    print(f"Total length\t\t\t\t: {costs['ten']}")
