## Arrays and Linked List
* When program declare a variable, they essentially ask the computer to allocate a chunk of memory, the program then can use this chunk of memory to store values such as numbers, characters, etc.

* Arrays and Linked list are data structure to store values in memory.

* Array values/elements occupy contiguous memory space/address.

* Elements in an array can be accessed using their index. 

* Array can't grow and shrink dynamically.

* Linked list elements/nodes are not stored in contiguous manner, each node may be in different memory space/address.

* Nodes in linked list store values and memory address of the next node.

## Selection Sort
* For each n elements in a list of items find the smallest/largest for n - 1 element in a list of items and place it to the correct position.

* Selection sort take O(n<sup>2</sup>) running times

## Exercise
2.1. List. Because the tracking apps will use a lot of insertion, it's best to use the constant time insertion in linked list, also if you use array you have to reserved some size it can cause a problem if you reach the limit or a lot of memory are wasted.

2.2. List

2.3. Array

2.4. Insertion can take O(n) times when: array is out of space or insert value in the middle of array.