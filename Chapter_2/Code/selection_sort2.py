def selection_sort(array: list) -> list:
    for i in range(len(array)):
        smallest_index_sofar = i
        for j in range(i + 1, len(array)):
            if array[j] < array[smallest_index_sofar]:
                smallest_index_sofar = j
        if i != smallest_index_sofar:
            array[i], array[smallest_index_sofar] = \
            array[smallest_index_sofar], array[i]
    return array

array = [6, 7, 1, 0, 10, 9, 8, 2, 5, 3, 4]
print("Unsorted array: ")
print(array)

print("Sorted array: ")
print(selection_sort(array))