function selectionSort(array) {
    for (let i = 0; i < array.length; i++) {
        let smallestIndexSofar = i;
        for (let j = i + 1; j < array.length; j++) {
            if (array[j] < array[smallestIndexSofar]) {
                smallestIndexSofar = j;
            }
        }

        if (i !== smallestIndexSofar) {
            let temp = array[i];
            array[i] = array[smallestIndexSofar];
            array[smallestIndexSofar] = temp;
        }
    }

    return array;
}

let array = [6, 7, 1, 0, 10, 9, 8, 2, 5, 3, 4];
console.log("Unsorted array: ");
console.log(array);

console.log("Sorted array: ");
console.log(selectionSort(array));