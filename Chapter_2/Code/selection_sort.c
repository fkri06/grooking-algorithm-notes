#include<stdio.h>

void swap(int* a, int* b){
	int temp = *a;
	*a = *b;
	*b = temp;
}

void selection_sort(int* array, int len) {
	for(int i = 0; i < len; i++) {
		int smallest_index_sofar = i;
		
		for(int j = i + 1; j < len; j++) {
			if(array[j] < array[smallest_index_sofar]) smallest_index_sofar = j;
		}

		if(i != smallest_index_sofar) swap(&array[i], &array[smallest_index_sofar]);

	}
}

void print_array(int* array, int len) {
	for(int i = 0; i < len; i++) {
		printf("%d ", array[i]);
	}
	printf("\n");
}

int main() {
	int array[] = {6, 7, 1, 0, 10, 9, 8, 2, 5, 3, 4};
	int length = sizeof(array) / sizeof(array[0]);

	printf("Unsorted array: \n");
	print_array(array, length);

	printf("Sorted array: \n");
	selection_sort(array, length);
	print_array(array, length);
}
