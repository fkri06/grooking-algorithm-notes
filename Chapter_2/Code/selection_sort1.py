def find_smallest(arr: list) -> int:
    # smallest value so far
    smallest = arr[0]
    smallest_index = 0

    for i in range(1, len(arr)):
        if arr[i] < smallest:
            # the new smallest
            smallest = arr[i]
            smallest_index = i 
    return smallest_index

def selection_sort(arr: list) -> list:
    new_arr = []
    for i in range(len(arr)):
        smallest = find_smallest(arr)
        new_arr.append(arr.pop(smallest))
    return new_arr

array = [6, 7, 1, 0, 10, 9, 8, 2, 5, 3, 4]
print("Unsorted array: ")
print(array)

print("Sorted array: ")
print(selection_sort(array))