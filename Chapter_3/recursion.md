## Recursion

* Function that calls itself.
* The calls has to stop in some condition.

## Base Case & Recursive Case

* Recursive case is when the function calls itself.
* Base case is the condition that is required in order for recursion to stop. Otherwise, it will run infinitely and crash the program.

## Stack

* Data structure that operates in LIFO(Last-In-First-Out) manners.


```
------------ Stack illustration ---------------

>>> push/add item onto the stack <<<<

        empty stack
            []

push(plate1) -----> [plate1]

push(plate2) -----> [plate2]
                    [plate1]

push(plate3) -----> [plate3]
                    [plate2]
                    [plate1]

push(plate4) -----> [plate4]
                    [plate3]
                    [plate2]
                    [plate1]

>>> pop/remove top most item from the stack <<<<

pop()   ----->      [plate3] ----> removed [plate4]
                    [plate2]
                    [plate1]

pop()   ----->      [plate2] ----> removed [plate3]
                    [plate1]

```


* Function call works in the same manner as stack. If a program calling function1, it will add the function onto the call stack in memory, when the program finish calling function1 it will pop out of the call stack.

* Call stack has a size limit.

## Exercise
3.2. The computer will run out of memory for the call stack.