def fact(n: int) -> int:
    if n == 1:
        return 1
    return n * fact(n - 1)

n = 5
print(f'{n}! = {fact(n)}')