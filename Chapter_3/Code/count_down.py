# function that takes integer n argument
# and count down to 0. This is an example of
# infinite recursion/stackoverflow, because
# there is no base case that will stop the function
# call.
def count_down(n: int):
    print(n)
    count_down(n - 1)

count_down(10)