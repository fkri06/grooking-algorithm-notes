## Divide & Conquer

* It's a recursive algorithm.
* A technique to solve problem by reducing large problem into smaller ones and solve the small problems.

## Quicksort

* Sorting list of items by dividing up large problems into smaller problems.
* Quicksort program needs **pivot** in order to divide one half of the list with the value less than the pivot, and the other half where the value greater than the pivot.
* It has O(n * log n) running time on average case and O(n<sup>2</sup>) in the worst case. The second n in both running time are the call stack height.


## Exercise

4.1. [Answer for 4.1](./Code/sum.py)

4.2. [Answer for 4.2](./Code/number_of_items.py)

4.3. [Answer for 4.3](./Code/find_max.py)

4.4. [Answer for 4.4](./Code/binary_search.py)

4.4. [Answer for 4.4](./Code/binary_search.py)

4.5. O(n)

4.6. O(n)

4.7. O(1)

4.8. O(n<sup>2</sup>)