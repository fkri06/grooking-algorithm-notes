# 
# the base case is when it found the value
# or when the low and high index overlap the index range
#
# the recursive case is if a value we're looking for is > or < the middle value
#  
def binary_search(haystack: list, needle: int, low: int, high: int):
    print("binary_search() called")
    if high <= -1 or low >= len(haystack):
        return False
    
    mid = (high + low) // 2
    mid_value = haystack[mid]

    if needle == mid_value:
        return True 
    elif needle < mid_value:
        return binary_search(haystack, needle, low, mid - 1)
    else:
        return binary_search(haystack, needle, mid + 1, high)

array = [1, 2, 3, 4, 5, 6, 7, 8]
search_value = 7
print(f"Does {search_value} exits in array {array}? {binary_search(array, search_value, 0, len(array) - 1)}")