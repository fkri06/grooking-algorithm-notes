def count_items(array: list):
    if not array:
        return 0
    return 1 + count_items(array[1:])

# 1, 2, 3, 4, 5
array = [number for number in range(1, 11)]
print(f"Array {array} has {count_items(array)} items")