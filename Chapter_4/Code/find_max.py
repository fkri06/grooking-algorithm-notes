from random import randint

def find_max(array: list):
    if len(array) == 1:
        return array[0]
    max_sofar = array[0]
    find_rest = find_max(array[1:])
    if find_rest > max_sofar:
        return find_rest
    else:
        return max_sofar

array = [randint(x, 100) for x in range(5)]
print(f"Maximum number from array {array} is {find_max(array)}")
