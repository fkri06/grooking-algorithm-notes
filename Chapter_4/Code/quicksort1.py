def quicksort(array):
    # if array empty or has 1 element
    if len(array) < 2:
        return array
    else:
        pivot = array[0]

        # partitioning 
        less = [i for i in array[1:] if i <= pivot]
        greater = [i for i in array[1:] if i > pivot]
        
        return quicksort(less) + [pivot] + quicksort(greater)

print(quicksort([5, 2, 1, 4, 3, 0]))