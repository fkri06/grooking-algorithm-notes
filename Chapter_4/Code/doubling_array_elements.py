# if we have an array [1, 2, 3, 4, 5]
# and double the elements, it'll become
# [2, 4, 6, 10]

## This is a in-place recursive solution
## for doubling elements in an array.
# def double_elements(array: list):
#     if not array:
#         return array
#     return [array[0] * 2] + double_elements(array[1:])

# in-place recursive solution
def double_elements(array: list, index = 0):
    if index >= len(array):
        return
    array[index] = array[index] * 2
    double_elements(array, index + 1)

array = [1, 2, 3, 4, 5]
print(id(array))
double_elements(array=array)
print(id(array))
print(array)
print(array)