def sum_array(array: list):
    if not array: # base case when array is empty
        return 0
    return array[0] + sum_array(array[1:])

array = [number for number in range(1, 10)]
print(f'sum of array {array} = {sum_array(array)}')