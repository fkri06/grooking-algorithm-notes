## Hash Table

* A data structure that store values using hash function to decide where the values should be stored in an array/table. 
* Use cases:
    * Lookup table
    * Preventing duplicate entries
    * Caching


## Hash Function

* A function that maps a data called keys to a value.
* Hash function requirements:
    * Consistent. If a key "Tiger" maps to a value 5, it must be always output a value of 5 when a program input "Tiger".

## Collision

* A collision happens when a hash function generates same data for different keys, thus overwritten the values in old data.

```
tiger => 5
hash_function(tiger) ---> return 0

store the value of tiger in index 0 of an array/table

Hash Table
[5, ]

bear => 7
hash_function(bear) ---> return 1

store the value of bear in index 1 of an array/table.

Hash Table
[5, 7]

Tapir => 3
hash_function(tapir) ---> return 0

store the value of tapir in index 0 of an array/table, but the slot 0 has already occupied by Tiger, this is a collision.
```
* Resolve a collision using linked-list

```
hash_function(Tiger) = 0
hash_function(Bear) = 1

[*, 7]
 |
 |
[{tiger: 5}, next]
 |
 |
[{tapir: 3}, NULL]

```

## Load Factor

* Calculate the number of empty slots in an array/table. 
    * load factor = number of items / total slot in a table.
* If load factor > 1, resize the table. Use 0.7 as resizing factor.
